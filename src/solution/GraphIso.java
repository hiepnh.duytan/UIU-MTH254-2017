package solution;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class GraphIso {

	public static int n_pairs = 10;
	public static int MAX_NODES = 11;
	public static int MIN_NODES = 5;
	
	////
	public static int[][] generateGraph(int n, int m){
		int[][] g = new int[n][n];
		
		Random random = new Random();
		for (int i = 0; i < m; i++){
			int e = random.nextInt(n*n);
			int u = e / n;
			int v = e % n;
			while (v == u || g[u][v] == 1){
				e = random.nextInt(n*n);
				u = e / n;
				v = e % n;
			}
			g[u][v] = 1;
			g[v][u] = 1;
		}
		
		//
		return g;
	}
	
	public static int[][] generateIso(int[][] G1){
		int n = G1[0].length;
		int[][] G2 = new int[n][n];
		for (int u = 0; u < n; u++)
			for (int v = 0; v < n; v++)
				G2[u][v] = G1[u][v];
		
		Random random = new Random();
		int n_swap = 1 + random.nextInt(3);
		//
		for (int i = 0; i < n_swap; i++){
			int u = random.nextInt(n-1);
			int v = u + 1 + random.nextInt(n-u-1);
			// swap u, v
			// rows u,v
			for (int t = 0; t < n; t++){
				int tmp = G2[u][t];
				G2[u][t] = G2[v][t];
				G2[v][t] = tmp;
			}
			// columns u,v
			for (int t = 0; t < n; t++){
				int tmp = G2[t][u];
				G2[t][u] = G2[t][v];
				G2[t][v] = tmp;
				
			}
			
		}
		
		//
		return G2;
	}
	
	public static void printGraph(int[][] G){
		int n = G[0].length;
		System.out.print("n = " + n + " : ");
		for (int u = 0; u < n; u++)
			for (int v = u+1; v < n; v++)
				if (G[u][v] == 1)
					System.out.print("(" + u + "," + v + ") ");
		System.out.println();
	}
	
	public static void generateGraphPairs() throws IOException{
		
		Random random = new Random();
		
		for (int i = 0; i < n_pairs; i++){
			System.out.println("pair " + i);
			//
			int isoType = random.nextInt(4);	// 0: different #nodes, 1: different #edges, 2: non-isomorphic, 3: isomorphic
			System.out.println("isoType = " + isoType);
			//
//			int n1 = MIN_NODES + random.nextInt(MAX_NODES - MIN_NODES);
//			int n2 = n1;
//			if (isoType == 0)
//				n2 = n1 - 1;
//			int m1 = 3*n1;
//			int m2 = 3*n2;
//			if (isoType == 1)
//				m2 = m1 - 2;
			
			// init two iso graph
			int n1 = MIN_NODES + random.nextInt(MAX_NODES - MIN_NODES);
			int n2 = n1;
			int m1 = 2*n1;
			int m2 = m1;
			System.out.println("n1 = " + n1 + " m1 = " + m1);
			
			int[][] G1 = generateGraph(n1, m1);
			System.out.println("G1 generated");
			printGraph(G1);
			
			int[][] G2 = generateIso(G1);
			System.out.println("G2 generated");
			printGraph(G2);
			
			// modify G2
			if (isoType == 0){
				n2 = n2 - 1;
				// delete node n2
				for (int v = 0; v < n2; v++){
					G2[n2][v] = 0;
					G2[v][n2] = 0;
				}
			}
			
			if (isoType == 1){
				m2 = m2 - 1;
				// delete one edge
				boolean found = false;
				for (int u = 0; u < n2 && ! found; u++){
					for (int v = u+1; v < n2; v++)
						if (G2[u][v] == 1){
							G2[u][v] = 0;
							G2[v][u] = 0;
							found = true;
							break;
						}
				}
			}
				
			if (isoType == 2){
				// swap two edges
//				int e = random.nextInt(n2*n2*n2*n2);
//				int e1 =  e / (n2*n2);
//				int e2 =  e % (n2*n2);
//						
//				int u = e1 / n2;
//				int v = e1 % n2;
//				int t = e2 / n2;
//				int w = e2 % n2;
//				
//				boolean found = (G2[u][v] == 1) && (G2[t][w] == 1) && (G2[u][t]) == 0 && (G2[v][w] == 0);
//				while (! found){
//					e = random.nextInt(n2*n2*n2*n2);
//					e1 =  e / (n2*n2);
//					e2 =  e % (n2*n2);
//							
//					u = e1 / n2;
//					v = e1 % n2;
//					t = e2 / n2;
//					w = e2 % n2;
//				}
//				//
//				G2[u][v] = 0; G2[v][u] = 0; 
//				G2[t][w] = 0; G2[w][t] = 0; 
//				G2[u][t] = 1; G2[t][u] = 1; 
//				G2[v][w] = 1; G2[w][v] = 1;
				
				// WAY 2
				boolean found = false;
				for (int u = 0; u < n2 && ! found; u++)
					for (int v = u+1; v < n2 && ! found; v++)
						for (int t = v+1; t < n2 && ! found; t++)
							for (int w = t+1; w < n2 && ! found; w++)
								if ((G2[u][v] == 1) && (G2[t][w] == 1) && (G2[u][t]) == 0 && (G2[v][w] == 0)){
									found = true;
									G2[u][v] = 0; G2[v][u] = 0; 
									G2[t][w] = 0; G2[w][t] = 0; 
									G2[u][t] = 1; G2[t][u] = 1; 
									G2[v][w] = 1; G2[w][v] = 1;
								}
				
			}
			
			// write to G1-i.txt, G2-i.txt
			BufferedWriter f = new BufferedWriter(new FileWriter("G1-" + i + ".txt"));
			f.write(n1 + "\n");
			for (int u = 0; u < n1; u++)
				for (int v = u+1; v < n1; v++)
					if (G1[u][v] == 1)
						f.write(u + " " + v + "\n");
			f.close();
			
			f = new BufferedWriter(new FileWriter("G2-" + i + ".txt"));
			f.write(n2 + "\n");
			for (int u = 0; u < n2; u++)
				for (int v = u+1; v < n2; v++)
					if (G2[u][v] == 1)
						f.write(u + " " + v + "\n");
			
			f.close();
		}
		
	}
	
	
	public static boolean isIso(int[][] G1, int m1, int[][] G2, int m2){
		//
		int n1 = G1.length;
		int n2 = G2.length;
		if (n1 != n2){
			System.out.println("NO. different #nodes.");
			return false;
		}
		if (m1 != m2){
			System.out.println("NO. different #edges.");
			return false;
		}
		
		// permutation
		int[] map = new int[n1];
		for (int i = 0; i < n1; i++)
			map[i] = i;
		
		boolean found = false;
		while (true){
			// check iso
			boolean trueMap = true;
			for (int u = 0; u < n1 && trueMap; u++)
				for (int v = u+1; v < n1 && trueMap; v++)
					if (G1[u][v] == 1 && G2[map[u]][map[v]] == 0)
						trueMap = false;
			if (trueMap == true){
				found = true;
				break;
			}
			
			// next permutation
			int j = n1-2;
			while (j >= 0 && map[j] > map[j+1])
				j--;
			if (j == -1)	// last permutation
				break;
			int k = n1-1;
			while (map[j] > map[k])
				k--;
			int tmp = map[k];
			map[k] = map[j];
			map[j] = tmp;
			int r = n1-1;
			int s = j+1;
			while (r > s){
				tmp = map[r];
				map[r] = map[s];
				map[s] = tmp;
				r--;
				s++;
			}
				
		}
		
		if (found){
			System.out.print("YES. map = ");
			for (int i = 0; i < n1; i++)
				System.out.print(map[i] + " ");
			System.out.println();
		}else{
			System.out.println("NO.");
		}
		
		//
		return found;
	}
	
	public static int[][] readGraph(String graphName) throws IOException{
		BufferedReader f = new BufferedReader(new FileReader(graphName));
		
		String str = f.readLine();
		int n = Integer.parseInt(str);
		int[][] g = new int[n][n];
		while (true){
        	str = f.readLine();
        	if (str == null)
        		break;
        	String[] items = str.split(" ");
        	int u = Integer.parseInt(items[0]);
        	int v = Integer.parseInt(items[1]);
        	g[u][v] = 1;
        	g[v][u] = 1;
        	// num edges
        	g[0][0] ++;
        }
		
		f.close();
		
		//
		return g;
	}
	
	public static void checkIso() throws IOException{
		
		
		for (int i = 0; i < n_pairs; i++){
			System.out.println("Pair " + i);
			
			int[][] G1 = readGraph("G1-" + i + ".txt");
			printGraph(G1);
			int[][] G2 = readGraph("G2-" + i + ".txt");
			printGraph(G2);
			//
			int n1 = G1.length;
			int n2 = G2.length;
			int m1 = G1[0][0];
			int m2 = G2[0][0];
			isIso(G1, m1, G2, m2);
			System.out.println("n1 = " + n1 + " m1 = " + m1 + ", n2 = " + n2 + " m2 = " + m2);
			
		}
		
	}
	
	
	public static void main(String[] args) throws Exception{
//		generateGraphPairs();
		
		checkIso();
		
	}

}
