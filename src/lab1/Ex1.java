package lab1;

public class Ex1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		boolean p = true;
//		boolean q = true;
//		
//		System.out.println("p AND q = " + (p && q)   );
//		System.out.println("p OR q = " + (p || q)   );
//		System.out.println("p XOR q = " + ((p && !q) || (!p && q))    );
//		System.out.println("p -> q = " + (!p || q)    );
//		System.out.println("p <-> q = " + ((p && q) || (!p && !q))   );
		
		//
		boolean[] boolArr = new boolean[]{true, false};
		
		System.out.println("p \t q \t AND \t OR \t XOR \t -> \t <->");
		for (boolean p : boolArr)
			for (boolean q : boolArr)
				System.out.println(p + "\t" + q + "\t" + 
						(p && q) + "\t" + 
						(p || q) + "\t" + 
						((p && !q) || (!p && q)) + "\t" + 
						(!p || q) + "\t" + 
						((p && q) || (!p && !q))  );
				
				
	}

}
