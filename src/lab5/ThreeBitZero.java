package lab5;

// Exercise 8 (Section 8.1)
public class ThreeBitZero {
	
	static int countThreeBitOne(int n){
		int[] a = new int[n];
		
		int count = 0;
		while (true){
//			for (int i = 0; i < n; i++)
//				System.out.print(a[i]);
//			System.out.println();
			
			// check a containing 3 consecutive 1-bits or not
			for (int i = 0; i < n-2; i++)
				if (a[i] == 0 && a[i+1] == 0 && a[i+2] == 0){
					count += 1;
					break;
				}
			
			// next bit string
			int i = n-1;
			while (i >= 0 && a[i] == 1)
				i = i-1;
			if (i == -1)
				break;
			
			a[i] = 1;
			for (int j = i+1; j < n; j++)
				a[j] = 0;
			
		}
		return count;
	}
	
	public static void main(String[] args) {
		for (int n = 3; n < 10; n++)
			System.out.println(n + "\t" + countThreeBitOne(n));
	}
}
